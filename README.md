# Floating-Window

## Description

Simple code in jQuery/javascript which provides windows floating after hover over item

## Usage
1. Create floating div with any id and class="floating-window"
1. Create item with attributes (see demo):
	* id
	* floating-window-id - id of div you want to float
	* onmouseout="hideWindow(this)"
	* onmouseover="showWindow(this)" OR onmouseover="showWindowOnCursor(this)"
1. Enjoy